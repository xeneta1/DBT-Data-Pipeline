# DBT-Data-Pipeline

The model consists of 3 schemas raw, staging and final. The schema names depend on the environment and are named accordingly. Eg.dev_raw for dev environment.
The initial and incremental load into the raw schema tables is done through python scripts so there's no "raw" folder inside the "models" folder.
The python script for initial load will do nothing if data has been already loaded for the first time. The incremental load uses a delete-insert strategy to load data for datapoints and charges tables.
Models:
The models folder contains sub-folders staging and final for each schema respectively. Staging schema contains tables which use the raw schema tables as a source. The final schema contains one aggregated table and one view for reporting alongwith 2 ephemeral models for checking data quality and coverage.
Macros:
The macro contains helper functions to insert coverage data after each run and also heps in calculating the upper and lower date bounds for datapoints.

## Getting started (to use this repo manually see below steps)

# 1. Create a python virtual environment
python -m venv venv

# 2. Activate the virtual environment
for windows:
venv\Scripts\activate

for mac:
source venv/bin/activate

# 3. Install the required modules
pip install -r requirements.txt

# 4. Command to run python module to do the initial load (environment name is provided as sys argument). This is only needed for one time for each environment
python load_initial_data.py <env>
Eg. python load_initial_data.py dev
Run the dbt models after the initial load to make sure all required tables are created
dbt run -t <env>

# 5. Additional data can be loaded by placing csv files in incremental_load folder and running below command (right now only datapoints and charges is supported) 
python incremental_load.py <env>
Eg. python incremental_load.py dev

# 6. Perform tests on dbt
dbt test -t <env>
Eg. dbt test -t dev

# 7. Command to run dbt models (environment can be dev or prod)
dbt run -t <env>
Eg. dbt run -t dev

# Additional info
Config folder stores configuration for tables to be used for incremental load. It has unique keys and columns defined to be used to prepare a merge statement. In case of DuckDB, however we can only do a delete and insert.

Sample queries folder contains example of queries that can be used get information for analysis or reports.


# Running sample queries
1. Get average container shipping price of equipment type 2 from China East Main region to US West Coast region
![alt text](image.png)

2. Get all lanes with a daily total price of equipment type 1 greater than 6000 USD
![alt text](image-1.png)

3. Monthly trend of median prices for all lanes
![alt text](image-2.png)

4. Top 5 lanes with highest prices
![alt text](image-3.png)