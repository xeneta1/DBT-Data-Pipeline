-- incremental model to store the charges
{{ 
  config(
    materialized='incremental',
    unique_key=['d_id', 'currency', 'charge_value'],
  ) 
}}
with validated_charges as (
    select
        d_id,
        currency,
        charge_value,
        current_timestamp as added_on
    from 
    {{ source('xeneta', 'charges') }}
)

select * from validated_charges
