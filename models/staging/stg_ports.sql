-- models/staging/stg_ports.sql
with validated_ports as (
    select
        pid,
        code as port_code,
        slug as region_slug,
        name as port_name,
        country,
        country_code
    from {{ source('xeneta', 'ports') }}
)

select * from validated_ports
