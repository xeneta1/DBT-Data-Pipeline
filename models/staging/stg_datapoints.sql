-- incremental model for storing datapoints
{{ 
  config(
    materialized='incremental',
    unique_key='d_id',
  ) 
}}

with validated_datapoints as (
    select
        d_id,
        created,
        origin_pid,
        destination_pid,
        valid_from,
        valid_to,
        company_id,
        supplier_id,
        equipment_id,
        current_timestamp as added_on
    from {{ source('xeneta', 'datapoints') }}

    {% if is_incremental() %}

    where created > (select max (created) from {{this}})  -- consider only the datapoints which are newly created or which the data has been received newly

    {% endif %}
)

select * from validated_datapoints
