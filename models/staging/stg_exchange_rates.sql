-- incremental model for storing the exchange rates
{{ 
  config(
    materialized='incremental',
    unique_key=['date', 'currency'],
  ) 
}}
with validated_exchange_rates as (
    select
        day as date,
        currency,
        rate as exchange_rate
    from {{ source('xeneta', 'exchange_rates') }}
)

select * from validated_exchange_rates
