-- models/staging/stg_regions.sql
with validated_regions as (
    select
        slug as region_slug,
        name as region_name,
        parent as parent_region
    from {{ source('xeneta', 'regions') }}
)

select * from validated_regions
