-- this is a view for reporting to analyse the aggregated prices for datapoints for different lanes
{{ 
  config(
    materialized='view'
  ) 
}}

with datapoints_report as (
    select
        date,
        CONCAT(origin_region, '-', destination_region) as lane,
        total_price_in_usd,
        avg_price_in_usd,
        median_price_in_usd,
        equipment_id
    from {{ ref('datapoints_aggregated_by_equipment') }}
    where is_datapoint_valid = true and dq_ok = true
)
select * from datapoints_report
