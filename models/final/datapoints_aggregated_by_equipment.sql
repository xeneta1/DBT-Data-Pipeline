-- this model aggregates data for all datapoints and groups them by date, origin, destination and equipment type
{{ 
  config(
    materialized='incremental',
    unique_key='d_id',
  ) 
}}


-- get all consecutive dates between the min and max contract dates of all datapoints
with recursive dates AS (
    {% set start_date = get_lower_date() %}
    {% set end_date = get_upper_date() %}
    select '{{ start_date }}'::date as date
    UNION ALL
    SELECT date + INTERVAL 1 DAY
    FROM dates
    WHERE date < '{{ end_date }}'::date
),-- calculate the max date present for each currency in the rates table
exch_rate as (
    select date, 
    currency, 
    exchange_rate, 
    MAX(date) OVER (partition by CURRENCY) as max_date 
    from {{ ref('stg_exchange_rates') }}
),
price_in_usd as (
    select
        charges.d_id,
        dp.origin_pid,
        origin_p.port_name as origin_port,
        origin_r.region_name as origin_region,
        dp.destination_pid,
        dest_p.port_name as destination_port,
        dest_r.region_name as destination_region,
        dp.equipment_id,
        charges.charge_value / coalesce(ex_rate_ori.exchange_rate, ex_rate_est.exchange_rate)  as usd_value,-- use exchange rate for the latest date if value for the current date is not present
        case 
            when dates.date between 
            cast('{{ var('valid_from', '2021-01-01') }}' as date) 
            and cast('{{ var('valid_to', '2022-06-01') }}' as date)
            then true else false 
        end as is_datapoint_valid,
        case
            when scc.num_companies >= {{ var('min_company_threshold', 5) }} 
            and scc.num_suppliers >= {{ var('min_supplier_threshold', 2) }} then true
            else false
        end as dq_ok,
        dp.valid_from,
        dp.valid_to,
        dates.date
    from dates
    join {{ ref('stg_datapoints') }} dp
    on dates.date between dp.valid_from and dp.valid_to
    join {{ ref('stg_charges') }} charges
    on charges.d_id = dp.d_id
    left join exch_rate ex_rate_ori -- left join as we need all future dates for the contract which might not be present in rates table 
    on (charges.currency = ex_rate_ori.currency
    and dates.date = ex_rate_ori.date)
    join exch_rate ex_rate_est  -- this join would give us the estimated exchange rate which is nothing but the rate at latest date
    on (charges.currency = ex_rate_est.currency
    and ex_rate_est.date = ex_rate_est.max_date)    
    join {{ ref('supplier_company_counts') }} scc
    on dp.origin_pid = scc.origin_pid
    and dp.destination_pid = scc.destination_pid
    and dp.equipment_id = scc.equipment_id
    join {{ ref('stg_ports') }} origin_p
    on origin_p.pid = dp.origin_pid
    join {{ ref('stg_ports') }} dest_p
    on dest_p.pid = dp.destination_pid
    join {{ ref('stg_regions') }} origin_r 
    on origin_r.region_slug = origin_p.region_slug
    join {{ ref('stg_regions') }} dest_r 
    on dest_r.region_slug = dest_p.region_slug
),
daily_aggregates as (   -- aggregates all datapoints and calculates avg, median and total price in usd for each day by lane and equipment type
    select
        date,
        d_id,
        origin_pid,
        origin_port,
        origin_region,
        destination_pid,
        destination_port,
        destination_region,
        equipment_id,
        sum(usd_value) as total_price_in_usd,
        avg(usd_value) as avg_price_in_usd,
        median(usd_value) as median_price_in_usd,
        is_datapoint_valid,
        dq_ok
    from price_in_usd
    group by 
        date, 
        d_id,
        origin_pid, 
        origin_port,
        origin_region,
        destination_pid,
        destination_port,
        destination_region, 
        equipment_id,
        is_datapoint_valid,
        dq_ok
)

select * from daily_aggregates
