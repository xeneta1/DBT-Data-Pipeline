-- this model calculates the count of covered lane combinations with dq_ok = true and is then used by macro to insert the data after each macro run
{{ 
  config(
    materialized='ephemeral',
  ) 
}}

with covered_data as (
    select
        origin_pid,
        destination_pid,
        equipment_id,
        date
    from {{ ref('datapoints_aggregated_by_equipment') }} 
    where dq_ok = true
    group by origin_pid, destination_pid, equipment_id, date
)

select
    count(*) as covered_lane_count
from covered_data
