-- this model calculates the number of companies and number of suppliers to check for sufficient coverage while aggregating the datapoints
{{ 
  config(
    materialized='ephemeral'
  ) 
}}

with supplier_company_counts as (
    select
        origin_pid,
        destination_pid,
        equipment_id,
        count(distinct company_id) as num_companies,
        count(distinct supplier_id) as num_suppliers
    from {{ ref('stg_datapoints') }}
    group by origin_pid, destination_pid, equipment_id
)
select * from supplier_company_counts
