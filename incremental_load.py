import duckdb
import os
import sys
import json

# Get the environment
if len(sys.argv) > 1:
    environment = sys.argv[1]
else:
    environment = 'dev'  # Default value

current_directory = os.getcwd()
incremental_folder = os.path.join(current_directory,'incremental_load')
config_folder = os.path.join(current_directory,'config')
data_files = {}
for filename in os.listdir(incremental_folder):
    if 'datapoints' in filename:
        data_files[filename] = 'datapoints'
    if 'charges' in filename:
        data_files[filename] = 'charges'

def create_delete_script(target_table: str, source_table: str, profile: dict):
    source_alias = 'SOURCE'
    target_alias = 'TARGET'
    delete_condition = ' and '.join([target_alias + '.' + attr["name"] + ' = ' + source_alias + '.' + attr["name"] for
         attr in
         profile["merge"]["primary_key"]])
    
    stmt = f'delete from {target_table} AS {target_alias}\n'
    stmt += f'using ({source_table}) AS {source_alias}\n'
    stmt += f'where ({delete_condition})\n'

    return stmt

# Connect to DuckDB
with duckdb.connect(f'{environment}.duckdb') as conn:

    # Load each file into the corresponding table
    for file, table in data_files.items():
        file_path = os.path.join(incremental_folder, file)
        config_file_path = os.path.join(config_folder, f'{table}.json')
        with open(config_file_path, "r") as _file:
            config_file = json.loads(_file.read())
        target_table = f"{environment}_raw.{table}"
        source_table = f"SELECT * FROM read_csv_auto('{file_path}')"
        print(create_delete_script(target_table, source_table, config_file))
        if os.path.exists(file_path):
            # performing a delete and insert as merge is not supported by duckdb
            conn.execute(create_delete_script(target_table, source_table, config_file))
            conn.execute(f"""
                INSERT INTO {environment}_raw.{table}
                SELECT * FROM read_csv_auto('{file_path}')
            """)

    print("Data loaded successfully.")
