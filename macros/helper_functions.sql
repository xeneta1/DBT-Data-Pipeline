-- this macro will insert the number of `(origin, destination, equipment type, day)` combinations with `dq_ok = True`
-- this would be called through pre and post hooks in the final aggregated datapoint model
{% macro create_coverage_metadata() %}

    {% set table_name = 'coverage_metadata' %}

    {% set schema_name = '' %}

    {% if target.name == 'dev' %}
        {% set schema_name = 'dev_raw' %}
    {% else %}
        {% set schema_name = 'prod_raw' %}
    {% endif %}

    {% set columns_definition = 'covered_lane_count INT, added_on datetime' %}

    -- Create table if it does not exist
    create table if not exists {{ schema_name }}.{{ table_name }} (
        {{ columns_definition }}
    )

{% endmacro %}


{% macro insert_coverage_metadata() %}
    
    {% set table_name = 'coverage_metadata' %}

    {% set schema_name = '' %}

    {% if target.name == 'dev' %}
        {% set schema_name = 'dev_raw' %}
    {% else %}
        {% set schema_name = 'prod_raw' %}
    {% endif %}

    {% set columns_definition = 'covered_lane_count INT, added_on datetime' %}

    -- Insert data into the table
    insert into {{ schema_name }}.{{ table_name }} (covered_lane_count,added_on)
        select covered_lane_count, current_timestamp as added_on
        from  {{ ref('coverage') }}
    
{% endmacro %}


{% macro get_lower_date() %}

    {% set query %}
    select min(valid_from) as min_valid_from from {{ ref('stg_datapoints') }}
    {% endset %}

    {% if execute %}
    {% set min_valid_from = run_query(query).columns[0][0] %}
    {% else %}
    {% set min_valid_from = "" %}
    {% endif %}
    {% do return(min_valid_from) %}

{% endmacro %}

{% macro get_upper_date() %}

    {% set query %}
    select max(valid_to) as max_valid_to from {{ ref('stg_datapoints') }}
    {% endset %}

    {% if execute %}
    {% set max_valid_to = run_query(query).columns[0][0] %}
    {% else %}
    {% set max_valid_to = "" %}
    {% endif %}
    {% do return(max_valid_to) %}

{% endmacro %}