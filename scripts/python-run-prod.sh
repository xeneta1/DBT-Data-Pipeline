# Check for new files in incremental_load folder
if [ $(git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA | grep "^incremental_load/" | wc -l) -gt 0 ]; then
    echo "New files detected in incremental_load. Triggering Python script to load incremental data."
    python incremental_load.py dev 
else
    echo "No new files detected in incremental_load. Skipping Python script."
fi