# Find modified dbt models and upstream dependencies
MODIFIED_MODELS=$(git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA | grep 'models/' | sed 's/models\///' | sed 's/.sql//')
if [ -n "$MODIFIED_MODELS" ]; then
    dbt run --models "$MODIFIED_MODELS"+ --profiles-dir "$DBT_PROFILES_DIR" --target dev
else
    echo "No modified models detected."
fi