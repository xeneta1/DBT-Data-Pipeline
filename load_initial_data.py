import duckdb
import os
import sys

# Get the environment
if len(sys.argv) > 1:
    environment = sys.argv[1]
else:
    environment = 'dev'  # Default value
# Connect to DuckDB
with duckdb.connect(f'{environment}.duckdb') as conn:

    # Define the CSV files and their corresponding table names
    data_files = {
        "DE_casestudy_ports.csv": "ports",
        "DE_casestudy_regions.csv": "regions",
        "DE_casestudy_exchange_rates.csv": "exchange_rates",
        "DE_casestudy_datapoints_1.csv": "datapoints",
        "DE_casestudy_charges_1.csv": "charges"
    }

    #
    conn.execute(f'CREATE SCHEMA IF NOT EXISTS {environment}_raw')
    # Load each file into the corresponding table
    for file, table in data_files.items():
        file_path = os.path.join('data', file)
        try:
            conn.execute(f"""
                CREATE TABLE {environment}_raw.{table} AS 
                SELECT * FROM read_csv_auto('{file_path}')
            """)
        except duckdb.CatalogException as e:    # prevent loading of data more than one time
            if 'already exists' in str(e):
                print('Initial load already done')
            else:
                raise
        except Exception as e:
            print(f'An unexpected error occurred: {e}')
        

    print("Data loaded successfully.")
