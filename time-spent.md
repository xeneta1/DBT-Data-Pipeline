## Time spent on the case study

# Setting up everything
1 hour

# Prepare a logical model and create folder structure for dbt
2 hours

# Write python scripts for initial and incremental load
1 hour

# Write sql scripts for all models (including source and tests) for basic requirements
1 hour

# Write logic for additional requirements including learning about dbt hooks :)
1.5 hours

# Write a CI/CD pipeline
30 min

# Test all queries in DBeaver
20 min

# Fix issues after testing
20 min

# Documenting everything including screenshots for sample analysis queries
20 min


# Total
8 hours
