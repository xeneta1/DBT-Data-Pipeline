-- get the average container shipping price of equipment type _2_ from China East Main region to US West Coast region?
select date,
avg_price_in_usd 
from prod_final.datapoints_report
where equipment_id = 2 and lane = 'China East Main-US West Coast'


-- Get all lanes with a daily total price of equipment type 1 greater than 6000 USD
select distinct 
lane as transportaion_lane 
from prod_final.datapoints_report
where equipment_id = 1 and total_price_in_usd > 6000


-- Monthly trend of median prices for all lanes
select 
    date_trunc('month', date) AS month, 
    avg(median_price_in_usd) AS avg_median_price 
from 
    prod_final.datapoints_report 
group by 
    month 
order by 
    month;


--Top 5 lanes with highest prices
select 
    lane, 
    sum(total_price_in_usd) AS total_price 
from 
    prod_final.datapoints_report 
group by 
    lane 
order by 
    total_price desc 
limit 5;